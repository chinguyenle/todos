
import 'package:meta/meta.dart';
import 'package:todos/repository/todos_repository_core/todo_entity.dart';
import 'package:todos/repository/todos_repository_core/todos_repository.dart';
import 'package:todos/repository/todos_repository_simple/file_storage.dart';
import 'package:todos/repository/todos_repository_simple/web_client.dart';
class TodosRepositoryFlutter implements TodosRepository {
  final FileStorage fileStorage;
  final WebClient webClient;

  const TodosRepositoryFlutter({
    @required this.fileStorage,
    this.webClient = const WebClient()
  });

  @override
  Future<List<TodoEntity>> loadTodos() async {
    try {
      return await fileStorage.loadTodos();
    } catch (e) {
      final todos = await webClient.fetchTodo();
      fileStorage.saveTodos(todos);
      return todos;
    }
  }

  @override
  Future saveTodos(List<TodoEntity> todos) {
    return Future.wait<dynamic>([
      fileStorage.saveTodos(todos),
      webClient.postTodos(todos)
    ]);
  }
  
  
}