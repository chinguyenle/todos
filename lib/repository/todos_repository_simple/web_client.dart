import 'package:todos/repository/todos_repository_core/todo_entity.dart';

class WebClient {
  final Duration delay;
  const WebClient([this.delay = const Duration(milliseconds: 3000)]);

  Future<List<TodoEntity>> fetchTodo() async {
    return Future.delayed(
        delay,
        () => [
              TodoEntity(
                false,
                'Buy food for da kitty',
                'With the chickeny bits',
                '1',
              ),
              TodoEntity(
                false,
                'Find a Red Sea dive trip',
                'Echo vs MY Dream',
                '2',
              ),
              TodoEntity(
                true,
                'Book flights to Egypt',
                '',
                '3',
              ),
            ]);
  }

  Future<bool> postTodos(List<TodoEntity> todos) async {
    return Future.value(true);
  }
}
