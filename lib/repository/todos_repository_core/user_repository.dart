import 'dart:async';
import 'package:todos/repository/todos_repository_core/use_entity.dart';

abstract class UserRepository {
  Future<UserEntity> login();
}