class UserEntity {
  final String id;
  final String displayName;
  final String photoUrl;

  UserEntity({this.id, this.displayName, this.photoUrl});


  @override
  bool operator ==(Object other) =>
  identical(this, other)||
  other is UserEntity && 
  runtimeType == other.runtimeType &&
  id == other.id &&
  displayName == other.displayName &&
  photoUrl == other.photoUrl;

  int get hashCode => id.hashCode ^ displayName.hashCode ^ photoUrl.hashCode;

  String toString() {
    return 'Userentity {id: $id, displayName: $displayName, photoUrl: $photoUrl}';
  }

}