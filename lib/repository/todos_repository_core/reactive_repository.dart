import 'dart:async';
import 'dart:core';

import 'package:todos/repository/todos_repository_core/todo_entity.dart';

abstract class ReactiveTodosRepository {
  Future<void> addNewTodo(TodoEntity todo);
  Future<void> deleteTodo(List<String> idList);
  Stream<List<TodoEntity>> todos();
  Future<void> updateTodo(TodoEntity todo);
}