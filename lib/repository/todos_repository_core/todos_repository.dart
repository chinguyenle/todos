import 'dart:async';
import 'dart:core';
import 'package:todos/repository/todos_repository_core/todo_entity.dart';

abstract class TodosRepository {
  Future<List<TodoEntity>> loadTodos();

  Future saveTodos(List<TodoEntity> todos);
}