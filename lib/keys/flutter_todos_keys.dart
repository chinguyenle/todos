import 'package:flutter/widgets.dart';

class FlutterTodosKeys {
  static final extraActionsPopupMenuButton = const Key('__extraActionsPopupMenuButton__');
  static final extraActionsemptyContainer = const Key('__extraActionsEmptyContainer__');
  static final filteredTodosEmptyContainer = const Key('_filteredTodosEmptyContainer__');
  static final statsLoadingIndicator = const Key('__statsLoadingIndicator__');
  static final emptyStatscontainer = const Key('__emptyStatsContainer__');
  static final emptyDetailsContainer = const Key('__emptyDetailsContainer__');
  static final detailsScreenCheckBox = const Key('__detailsScreenCheckBox__');
}