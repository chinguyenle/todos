import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:todos/repository/todos_repository_core/todo_entity.dart';

class Todo extends Equatable {
  final bool complete;
  final String id;
  final String note;
  final String task;

  Todo(this.task, {this.complete= false, String note = '', String id}):this.note = note ?? '', this.id = id ?? Random.secure();

  Todo copyWith({bool complete, String id, String note, String task}) {
    return Todo(
      task??this.task,
      complete:  complete?? this.complete,
      id: id ?? this.id,
      note: note ?? this.note
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [ complete, id, note, task];

  String toString() {
    return 'Todo { complete: $complete, task: $task, note: $note, id: $id }';
  }

  TodoEntity todoEntity() {
    return TodoEntity(complete, task, note, id);
  }

  static Todo fromEntity(TodoEntity entity) {
    return Todo(
      entity.task,
      complete: entity.complete ?? false,
      note: entity.note,
      id: entity.id ?? Random.secure()
    );
  }

}